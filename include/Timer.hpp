// [[file:Timer.org::*Header][Header:1]]
#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono>
#include <vector>
#include <iostream>
#include <unordered_map>
#include <set>
#include <string>
#include <iomanip>
#include <mutex>
#include <limits>
#include <cmath>

#ifndef TIMER_LEVEL_MAX
#define TIMER_LEVEL_MAX 5
#endif

#ifndef TIMER_LEVEL_MIN
#define TIMER_LEVEL_MIN 0
#endif

#ifndef TIMER_THREAD_SAFE
#define TIMER_THREAD_SAFE 1
#endif

static_assert((TIMER_LEVEL_MAX >= TIMER_LEVEL_MIN));

namespace timer {
namespace {

#ifdef TIMER_RECORD_CUMULATIVE
static constexpr const bool timer_cumulative = true;
#else
static constexpr const bool timer_cumulative = false;
#endif
// Header:1 ends here

// [[file:Timer.org::*Global variables for thread safety][Global variables for thread safety:1]]
#if TIMER_THREAD_SAFE
  std::mutex _dict_mutex;
  std::set<std::string> _thread_lockers;
#endif
// Global variables for thread safety:1 ends here

// [[file:Timer.org::*Event class][Event class:1]]
std::chrono::time_point<std::chrono::high_resolution_clock> timer_t0 = std::chrono::high_resolution_clock::now();

struct Event{
  using instant = std::chrono::time_point<std::chrono::high_resolution_clock>;
  using duration = std::chrono::duration<double>;
  instant start_time;
  instant end_time;
  int level;

  Event(instant s, int l): start_time{s}, level{l} {}

  static void write_instant(const instant& t, std::ostream &out = std::cout, std::string suffix = ""){
    duration d = t - timer_t0;
    out << std::left << std::setw(10) << std::setprecision(10) << std::setfill(' ') << d.count() << suffix;
  }

  void write_debug(std::string_view name, bool finished = true, std::ostream &out = std::cout){
    for(int i = 0; i < level; ++i) out << "!  ";
    out << std::left << std::setw(24) << std::setfill(' ') << name;
    out << " | ";
    out << std::left << std::setw(4) << level;
    out << " | ";
    write_instant(start_time, out);
    out << " | ";
    if(finished){
      duration measure = end_time - start_time;
      out << std::left << std::setw(12) << std::setprecision(10) << std::setfill(' ') << measure.count() << " | ";
      write_instant(end_time, out, "\n");
    }
    else{
      out << std::left << std::setw(12) << std::setfill(' ') << ' ';
      out << " | ";
      out << std::left << std::setw(12) << std::setfill(' ') << ' ';
      out << " | \n";
    }
  }

  void trace(std::string_view name, std::ostream &out = std::cout){
    out << name << ',' << level << ',' ;
    write_instant(start_time, out, ",");
    write_instant(end_time, out, "\n");
  }
};
// Event class:1 ends here

// [[file:Timer.org::*CumulativeTimer][CumulativeTimer:1]]
class CumulativeTimer{
private:
  using instant = std::chrono::time_point<std::chrono::high_resolution_clock>;
  using duration = std::chrono::duration<double>;
  using Map = std::unordered_map<std::string, duration>;

  static Map _timers_dict;
#if TIMER_THREAD_SAFE
  bool _is_responsible = false;
#endif
  std::string _name;
  bool _started = false;
  bool _stopped = false;
  instant _start_time;

public:
  CumulativeTimer(const std::string& name):
    _name{name}
  {
    _started = true;
    _start_time = std::chrono::high_resolution_clock::now();
#if TIMER_THREAD_SAFE
    std::lock_guard<std::mutex> guard(_dict_mutex);
    if(_thread_lockers.count(name) == 0){
      _is_responsible = true;
      _thread_lockers.insert(name);
    }
#endif
    CumulativeTimer::_timers_dict.try_emplace(name, duration::zero());
  }

  void stop(){
    if (_started){
      duration measure = std::chrono::high_resolution_clock::now() - _start_time;
#if TIMER_THREAD_SAFE
      if(_is_responsible){
	std::lock_guard<std::mutex> guard(_dict_mutex);
	CumulativeTimer::_timers_dict[_name] += measure;
	_thread_lockers.erase(_thread_lockers.find(_name));
      }
#else
      CumulativeTimer::_timers_dict[_name] += measure;
#endif
      _stopped = true;
    }
  }

  ~CumulativeTimer(){
    if(!_stopped){
      stop();
    }
  }

  static void reset(){
#if TIMER_THREAD_SAFE
    std::lock_guard<std::mutex> guard(_dict_mutex);
#endif
    CumulativeTimer::_timers_dict.clear();
  }

  static void results(std::ostream &out = std::cout, [[maybe_unused]] const bool detailed = true){
    out << "\nCumulativeTimer results: " << CumulativeTimer::_timers_dict.size() << " timers recorded" << "\n\n";

    for(auto& [name, dur] : CumulativeTimer::_timers_dict){
      out << name << ", " << dur.count() << '\n';
    }
  }

  static void summary_results(std::ostream &out = std::cout){
    CumulativeTimer::results(out, false);
  }

  static void pretty_results(std::ostream &out = std::cout){
    out << "\nCumulativeTimer results: " << CumulativeTimer::_timers_dict.size() << " timers recorded" << "\n\n";

    constexpr const int valwidth = 11;
    int namewidth = 6;
    for(const auto& [name, _] : CumulativeTimer::_timers_dict){
      namewidth = std::max(namewidth, static_cast<int>(name.size()));
    }

    auto print_line = [&](){
      out << ' ';
      const size_t size_line = 7 + namewidth + valwidth;
      for(size_t k = 0; k < size_line; ++k) out << '-';
      out << '\n';
    };

    print_line();
    out << " | " << std::setw(namewidth) << "name"
	<< " | " << std::setw(valwidth) << "Time"
	<< " |\n";
    print_line();

    for(const auto& [name, dur] : CumulativeTimer::_timers_dict){
      const double time = dur.count();
      out << " | " << std::setw(namewidth) << name
	  << " | " << std::setw(valwidth) << std::setprecision(5) << std::scientific << time
	  << " |\n";
      print_line();
    }

    out << '\n';
  }

  static void stats_results(std::ostream &out = std::cout){
    CumulativeTimer::results(out, false);
  }

  static std::vector<std::string> get_event_keys(){
    std::vector<std::string> keys;
    for(auto kv : _timers_dict){
      keys.push_back(kv.first);
    }
    return keys;
  }

  static double get_event_cumul_time(const std::string& name){
    return CumulativeTimer::_timers_dict[name].count();
  }
}; // class CumulativeTimer

std::unordered_map<std::string, std::chrono::duration<double>> CumulativeTimer::_timers_dict;
// CumulativeTimer:1 ends here

// [[file:Timer.org::*Attributes][Attributes:1]]
#define _TIMER_NO_LIMIT_VALUE -1

  class TrueTimer{

   private:
    using instant = std::chrono::time_point<std::chrono::high_resolution_clock>;
    using duration = std::chrono::duration<double>;
    using Map = std::unordered_map<std::string, std::vector<Event>>;

    static int _cur_level;
#if TIMER_THREAD_SAFE
    bool _is_responsible = false;
#endif
    static Map _event_dict;
    static const int NO_LIMIT = _TIMER_NO_LIMIT_VALUE;
    static int _n_events;

    std::vector<Event> * _events_ptr;
    std::string _name;
    int _level;
    bool _started = false;
    bool _stopped = false;

   public:

    static bool debug;
    static bool trace;
    static int max_depth;
    static int max_events;
    static std::ostream * debug_output;
    static std::ostream * trace_output;
// Attributes:1 ends here

// [[file:Timer.org::*Execution time event recording][Execution time event recording:1]]
private:
  [[nodiscard]] inline bool record_event() const {
    return ((max_depth == NO_LIMIT  || _level < max_depth )  &&
            (max_events == NO_LIMIT || TrueTimer::_n_events < max_events ));
  }
// Execution time event recording:1 ends here

// [[file:Timer.org::*Constructor: start the timer][Constructor: start the timer:1]]
public:
    TrueTimer(const std::string& name):
      _name{name},
      _level{ TrueTimer::_cur_level }
    {

#if TIMER_THREAD_SAFE
      {
        std::lock_guard<std::mutex> guard(_dict_mutex);
        if(_thread_lockers.count(name) == 0){
          _is_responsible = true;
          _thread_lockers.insert(name);
        }
        else{
          return;
        }
      }
#endif

      TrueTimer::_cur_level++;
      if (record_event()){
        _started = true;
        instant start_time = std::chrono::high_resolution_clock::now();
        ++TrueTimer::_n_events;
        TrueTimer::_event_dict.try_emplace(name, std::vector<Event>());
        TrueTimer::_event_dict[name].emplace_back(start_time, _level);
        _events_ptr = &(_event_dict[name]);
        if(debug){
          Event &last_e = _events_ptr->back();
          last_e.write_debug(name, false, (*debug_output));
        }
      }
    }
// Constructor: start the timer:1 ends here

// [[file:Timer.org::*Destructor or stop: stops the timer][Destructor or stop: stops the timer:1]]
void stop(){
    if(!_is_responsible) return;
    if (_started){
      _stopped = true;
      Event &last_e = _events_ptr->back();
      last_e.end_time = std::chrono::high_resolution_clock::now();

      if(debug){
        last_e.write_debug(_name, true, (*debug_output));
      }
      if(trace){
        last_e.trace(_name, (*trace_output));
      }
    }
#if TIMER_THREAD_SAFE
    {
      std::lock_guard<std::mutex> guard(_dict_mutex);
      _thread_lockers.erase(_thread_lockers.find(_name));
    }
#endif
    TrueTimer::_cur_level--;
  }

  ~TrueTimer(){
    if(!_stopped){
      stop();
    }
  }
// Destructor or stop: stops the timer:1 ends here

// [[file:Timer.org::*Resetting timers][Resetting timers:1]]
static void reset(){
    if(_cur_level != 0){
      throw std::runtime_error("Timer::reset called when timers are still active");
    }
#if TIMER_THREAD_SAFE
    std::lock_guard<std::mutex> guard(_dict_mutex);
#endif
    TrueTimer::_n_events = 0;
    TrueTimer::_event_dict.clear();
  }
// Resetting timers:1 ends here

// [[file:Timer.org::*Display timer results][Display timer results:1]]
static void results(std::ostream &out = std::cout, const bool detailed = true){

  out << "\nTimer results: " << TrueTimer::_n_events << " events recorded" << "\n\n";

  for(auto it : TrueTimer::_event_dict){
    const std::string &name = it.first;
    const std::vector<Event> &list_e = it.second;

    auto total = duration::zero();
    for (const auto &e : list_e){
      duration measure = e.end_time - e.start_time;
      total += measure;
    }
    out << name << ", " << list_e.size() << ", " << total.count() << '\n';

    if(detailed){
      bool first = true;
      for (const auto &e : list_e){
	if(!first){
	  out << ", ";
	}
	else{
	  first = false;
	}
	duration measure = e.end_time - e.start_time;
	out << measure.count();
      }
      out << '\n';
    }
  }
}

static void summary_results(std::ostream &out = std::cout){
  TrueTimer::results(out, false);
}

static std::vector<Event> get_events(const std::string& name){ return _event_dict.at(name); }
static std::vector<std::string> get_event_keys(){
  std::vector<std::string> keys;
  for(auto kv : _event_dict){
    keys.push_back(kv.first);
  }
  return keys;
}
static double get_event_cumul_time(const std::string& name){
  std::vector<Event> events = _event_dict.at(name);
  auto total = duration::zero();
  for (const auto &e : events){
    duration measure = e.end_time - e.start_time;
    total += measure;
  }
  return total.count();
}

static void pretty_results(std::ostream &out = std::cout){
  out << "\nTimer results: " << TrueTimer::_n_events << " events recorded" << "\n\n";

  constexpr const int valwidth = 9;
  int namewidth = 6;
  for(const auto& [name, _] : TrueTimer::_event_dict){
    namewidth = std::max(namewidth, static_cast<int>(name.size()));
  }

  auto print_line = [&](){
    out << ' ';
    const size_t size_line = 19 + namewidth + 5 * valwidth;
    for(size_t k = 0; k < size_line; ++k) out << '-';
    out << '\n';
  };

  print_line();
  out << " | " << std::setw(namewidth) << "name"
      << " | " << std::setw(valwidth) << "#events"
      << " | " << std::setw(valwidth) << "Min"
      << " | " << std::setw(valwidth) << "Max"
      << " | " << std::setw(valwidth) << "Avg"
      << " | " << std::setw(valwidth) << "Std"
      << " |\n";
  print_line();

  for(const auto& [name, list_e] : TrueTimer::_event_dict){
    if(list_e.size() == 0) continue;

    double min = std::numeric_limits<double>::max();
    double max = double{0};
    double sum = double{0};
    double sum_sq = double{0}; // Sum of squares

    for(const auto &e : list_e){
      const duration measure = e.end_time - e.start_time;
      const double time = measure.count();
      min = std::min(time, min);
      max = std::max(time, max);
      sum += time;
      sum_sq += time * time;
    }

    const size_t n_events = list_e.size();
    const double avg = sum / static_cast<double>(n_events);
    const double std = std::sqrt(sum_sq / static_cast<double>(n_events) - (avg * avg));

    out << " | " << std::setw(namewidth) << name
	<< " | " << std::setw(valwidth) << n_events
	<< " | " << std::setw(valwidth) << std::setprecision(3) << std::scientific << min
	<< " | " << std::setw(valwidth) << std::setprecision(3) << std::scientific << max
	<< " | " << std::setw(valwidth) << std::setprecision(3) << std::scientific << avg
	<< " | " << std::setw(valwidth) << std::setprecision(3) << std::scientific << std
	<< " |\n";
    print_line();
  }

  out << '\n';
}

static void stats_results(std::ostream &out = std::cout){
  out << "\nTimer results: " << TrueTimer::_n_events << " events recorded" << "\n\n";

  for(const auto& [_, list_e] : TrueTimer::_event_dict){
    for(const auto &e : list_e){
      const duration measure = e.end_time - e.start_time;
    }
  }

  out << "name,nb_events,Min,Max,Avg,Std\n";

  for(const auto& [name, list_e] : TrueTimer::_event_dict){
    if(list_e.size() == 0) continue;

    double min = std::numeric_limits<double>::max();
    double max = double{0};
    double sum = double{0};
    double sum_sq = double{0}; // Sum of squares

    for(const auto &e : list_e){
      const duration measure = e.end_time - e.start_time;
      const double time = measure.count();
      min = std::min(time, min);
      max = std::max(time, max);
      sum += time;
      sum_sq += time * time;
    }

    const size_t n_events = list_e.size();
    const double avg = sum / static_cast<double>(n_events);
    const double std = std::sqrt(sum_sq / static_cast<double>(n_events) - (avg * avg));

    out  << name << ','
	 << n_events << ','
	 << min << ','
	 << max << ','
	 << avg << ','
	 << std << '\n';
  }

  out << '\n';
}

};
// Display timer results:1 ends here

// [[file:Timer.org::*Initializing static values][Initializing static values:1]]
std::unordered_map<std::string, std::vector<Event>> TrueTimer::_event_dict;
int TrueTimer::_n_events = 0;
int TrueTimer::_cur_level = 0;
bool TrueTimer::debug = false;
bool TrueTimer::trace = false;
int TrueTimer::max_depth = TrueTimer::NO_LIMIT;
int TrueTimer::max_events = TrueTimer::NO_LIMIT;
std::ostream * TrueTimer::debug_output = &std::cout;
std::ostream * TrueTimer::trace_output = &std::cout;
// Initializing static values:1 ends here

// [[file:Timer.org::*DummyTimer][DummyTimer:1]]
struct DummyTimer{
  DummyTimer(const std::string&) {}
  void stop() {}
};
// DummyTimer:1 ends here

// [[file:Timer.org::*Timer][Timer:1]]
template<int L>
class Timer{
  constexpr static const bool is_true_timer = ((L >= TIMER_LEVEL_MIN) && (L < TIMER_LEVEL_MAX));
  using timer_true_type = typename std::conditional<timer_cumulative, CumulativeTimer, TrueTimer>::type;
  using timer_type = typename std::conditional<is_true_timer,
                                               timer_true_type,
                                               DummyTimer>::type;

  timer_type t;
public:

  static const int NO_LIMIT = _TIMER_NO_LIMIT_VALUE;

  Timer(const std::string& name): t{name} {}

  inline void stop(){ t.stop(); }

  static void reset(){
    timer_true_type::reset();
  }

  static void results(std::ostream &out = std::cout, const bool detailed = true){
    timer_true_type::results(out, detailed);
  }

  static void summary_results(std::ostream &out = std::cout){
    timer_true_type::results(out, false);
  }

  static void pretty_results(std::ostream &out = std::cout){
    timer_true_type::pretty_results(out);
  }

  static void stats_results(std::ostream &out = std::cout){
    timer_true_type::stats_results(out);
  }

  static std::vector<Event> get_events(const std::string& name){
    return TrueTimer::get_events(name);
  }

  static std::vector<std::string> get_event_keys(){
    return timer_true_type::get_event_keys();
  }

  static double get_event_cumul_time(const std::string& name){
    return timer_true_type::get_event_cumul_time(name);
  }

  static void set_debug(const bool v){ TrueTimer::debug = v; }
  static void set_trace(const bool v){ TrueTimer::trace = v; }
  static void set_max_depth(const int v){ TrueTimer::max_depth = v; }
  static void set_max_events(const int v){ TrueTimer::max_events = v; }
  static void set_debug_output(std::ostream * v){ TrueTimer::debug_output = v; }
  static void set_trace_output(std::ostream * v){ TrueTimer::trace_output = v; }
};
// Timer:1 ends here

// [[file:Timer.org::*Footer][Footer:1]]
} // end namespace timer
} // end namespace <anonymous>

#endif // TIMER_HPP
// Footer:1 ends here
