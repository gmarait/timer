// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#define TIMER_RECORD_CUMULATIVE
#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"
#include "test_timer.hpp"

int main() {
  [[maybe_unused]] int wait_time_f1_1 = fct_a1();
  [[maybe_unused]] int wait_time_f2_1 = fct_d2();
  [[maybe_unused]] int wait_time_f1_2 = fct_a1();
  [[maybe_unused]] int wait_time_f2_2 = fct_d2();

  Timer<0>::results();
  Timer<0>::stats_results();
  Timer<0>::pretty_results();

  return 0;
}
