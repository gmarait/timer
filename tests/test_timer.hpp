#include <iostream>
#include <chrono>
#include <thread>
#include "Timer.hpp"

using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
using std::chrono::system_clock;
using namespace timer;

int fct_c111(){

  Timer<3> t("fct_c111");

  sleep_for(10ms);
  return 10;
}

int fct_b11(){

  Timer<2> t("fct_b11");

  sleep_for(100ms);
  int r = 0;
  for(int i = 0; i < 3; ++i){
    r += fct_c111();
  }
  return r + 100;
}

int fct_a1(){

  Timer<1> t("fct_a1");

  sleep_for(300ms);
  int r = 0;
  for(int i = 0; i < 3; ++i){
    r += fct_b11();
  }
  return r + 300;
}

int fct_e22(){

  Timer<2> t("fct_e22");

  sleep_for(50ms);
  return 50;
}

int fct_d2(){

  Timer<1> t("fct_d2");

  sleep_for(100ms);
  int r = 0;
  for(int i = 0; i < 3; ++i){
    r += fct_e22();
  }
  return r + 100;
}
