// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#define TIMER_RECORD_CUMULATIVE
#define TIMER_THREAD_SAFE 1
#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"
#include "test_timer.hpp"
#include <thread>

int main() {
  const int n_threads = 4;

  std::vector<std::thread> threads(n_threads);

  for(int i = 0; i < n_threads; ++i) threads[i] = std::thread(fct_a1);
  for(int i = 0; i < n_threads; ++i) threads[i].join();

  for(int i = 0; i < n_threads; ++i) threads[i] = std::thread(fct_d2);
  for(int i = 0; i < n_threads; ++i) threads[i].join();

  for(int i = 0; i < n_threads; ++i) threads[i] = std::thread(fct_a1);
  for(int i = 0; i < n_threads; ++i) threads[i].join();

  for(int i = 0; i < n_threads; ++i) threads[i] = std::thread(fct_d2);
  for(int i = 0; i < n_threads; ++i) threads[i].join();

  Timer<0>::results();

  return 0;
}
