// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"
#include "test_timer.hpp"

int main() {

  Timer<0>::set_trace(true);
  Timer<0>::set_debug(true);

  std::ofstream tracefile{"trace.csv"};
  Timer<0>::set_trace_output(&tracefile);
  [[maybe_unused]] int wait_time_f1_1 = fct_a1();
  [[maybe_unused]] int wait_time_f2_1 = fct_d2();
  [[maybe_unused]] int wait_time_f1_2 = fct_a1();
  [[maybe_unused]] int wait_time_f2_2 = fct_d2();

  Timer<0>::summary_results();
  tracefile.close();

  return 0;
}
