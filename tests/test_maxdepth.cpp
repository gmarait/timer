// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"

using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
using std::chrono::system_clock;
using namespace timer;

void f111(){
  Timer<1> t("f111");
  sleep_for(10ms);
}

void f11(){
  Timer<1> t("f11");
  for(int i = 0; i < 2; ++i){
    f111();
  }
  sleep_for(10ms);
}

void f1(){
  Timer<1> t("f1");
  for(int i = 0; i < 2; ++i){
    f11();
  }
  sleep_for(10ms);
}

int main() {

  const int max_depth = 2;
  Timer<0>::set_max_depth(max_depth);

  for(int i = 0; i < 2; ++i){
    f1();
  }

  int caught = 0;
  try{ std::vector<Event> evt = Timer<0>::get_events("f111"); }
  catch(const std::out_of_range& e){ caught++; }

  if(caught != 1) return 1;

  if(Timer<0>::get_events("f1").size()  !=  2) return 1;
  if(Timer<0>::get_events("f11").size()  !=  4) return 1;

  if(caught != 1) return 1;

  Timer<0>::results();

  return 0;
}
