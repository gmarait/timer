// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"

using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
using std::chrono::system_clock;
using namespace timer;

int f11(){
  Timer<1> t("f11");
  int k = 0;
  for(int i = 0; i < 1000; ++i){
    k = k + 1;
  }
  return k;
}

int f1(){
  Timer<1> t("f1");
  int k = 0;
  for(int i = 0; i < 1000; ++i){
    k += f11();
  }
  return k;
}

int main() {

  using duration = std::chrono::duration<double>;
  Timer<1> t("main");

  int k = 0;
  for(int i = 0; i < 1000; ++i){
    k += f1();
  }

  t.stop();

  std::cout << k << '\n';

  std::vector<Event> evt_main = Timer<0>::get_events("main");
  std::vector<Event> evt_f1   = Timer<0>::get_events("f1");
  std::vector<Event> evt_f11  = Timer<0>::get_events("f11");

  duration d_f1 = duration::zero();
  for (const auto &e : evt_f1){
    duration measure = e.end_time - e.start_time;
    d_f1 += measure;
  }

  duration d_f11 = duration::zero();
  for (const auto &e : evt_f11){
    duration measure = e.end_time - e.start_time;
    d_f11 += measure;
  }

  duration d_main = evt_main[0].end_time - evt_main[0].start_time;

  duration oh_main = d_main - d_f1;
  duration oh_f1 = d_f1 - d_f11;

  std::cout << "Overhead between main and f1: " << oh_main.count() << '\n';
  std::cout << "Overhead between f1 and f11: " << oh_f1.count() << '\n';

  double rel_oh_main = oh_main.count() / d_main.count() * 100;
  double rel_oh_f1 = oh_f1.count() / d_f1.count() * 100;

  std::cout << "Relative overhead (%) between main and f1: " << rel_oh_main << '\n';
  std::cout << "Relative overhead (%) between f1 and f11: " << rel_oh_f1 << '\n';

  Timer<0>::summary_results();

  return 0;
}
