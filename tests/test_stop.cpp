// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"

using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
using std::chrono::system_clock;
using namespace timer;

void long_function(){

  sleep_for(50ms);

  // Start t1
  Timer<1> t1("t1");

  sleep_for(50ms);

  // Start t2
  Timer<1> t2("t2");

  sleep_for(50ms);

  // Stop t2
  t2.stop();

  sleep_for(50ms);

  // Stop t1
  t1.stop();

  sleep_for(50ms);

}

int main() {

  Timer<0>::set_debug(true);

  long_function();

  std::cout << "t1 should be around (but more than) 150 ms\n";
  std::cout << "t2 should be around (but more than) 50 ms\n";

  Timer<0>::results();

  return 0;
}
