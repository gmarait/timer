// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"

using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
using std::chrono::system_clock;
using namespace timer;

void f(){
  Timer<1> t("f");
  sleep_for(10ms);
}

int main() {

  const int max_events = 22;
  Timer<0>::set_max_events(max_events);
  Timer<0>::set_debug(true);

  for(int i = 0; i < 100; ++i){
    f();
  }

  if(Timer<0>::get_events("f").size() !=  max_events) return 1;

  Timer<0>::results();

  return 0;
}
