// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 2

#include "Timer.hpp"
#include "test_timer.hpp"

int main() {

  Timer<0>::set_debug(true);
  int wait_time_f1_1 = fct_a1();
  int wait_time_f2_1 = fct_d2();
  int wait_time_f1_2 = fct_a1();
  int wait_time_f2_2 = fct_d2();

  Timer<0>::results();
  Timer<0>::summary_results();

  if(Timer<0>::get_events("fct_a1").size()   !=  2) return 1;
  if(Timer<0>::get_events("fct_d2").size()   !=  2) return 1;

  int caught = 0;
  try{ std::vector<Event> evt = Timer<0>::get_events("fct_b11"); }
  catch(const std::out_of_range& e){ caught++; }

  try{ std::vector<Event> evt = Timer<0>::get_events("fct_c111"); }
  catch(const std::out_of_range& e){ caught++; }

  try{ std::vector<Event> evt = Timer<0>::get_events("fct_e22"); }
  catch(const std::out_of_range& e){ caught++; }

  if(caught != 3) return 1;

  std::cout << "-----\n";
  std::cout << "Wait time (ms) f1_1: " << wait_time_f1_1 << '\n';
  std::cout << "Wait time (ms) f2_1: " << wait_time_f2_1 << '\n';
  std::cout << "Wait time (ms) f1_2: " << wait_time_f1_2 << '\n';
  std::cout << "Wait time (ms) f2_2: " << wait_time_f2_2 << '\n';

  std::cout << "-----\n";
  std::cout << "Wait time (ms) total f1: " << wait_time_f1_1 + wait_time_f1_2 << '\n';
  std::cout << "Wait time (ms) total f2: " << wait_time_f2_1 + wait_time_f2_2 << '\n';

  return 0;
}
