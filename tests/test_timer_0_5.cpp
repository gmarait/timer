// This file was generated from include/Timer.org
// Please modify the org file and use org-babel tangle to modify this file
#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>
#include <algorithm>

#define TIMER_LEVEL_MIN 0
#define TIMER_LEVEL_MAX 5

#include "Timer.hpp"
#include "test_timer.hpp"

int main() {

  Timer<0>::set_debug(true);
  int wait_time_f1_1 = fct_a1();
  int wait_time_f2_1 = fct_d2();
  int wait_time_f1_2 = fct_a1();
  int wait_time_f2_2 = fct_d2();

  Timer<0>::results();
  Timer<0>::summary_results();

  auto check_n_events = [](const std::string& str, const size_t n_evt){
    if(Timer<0>::get_events(str).size() != n_evt){
      std::cerr << "Wrong number of events for "
                << str << " : " << Timer<0>::get_events(str).size()
                << " instead of " << n_evt << '\n';
      return true;
    }
    return false;
  };

  if(check_n_events("fct_a1", 2)) return 1;
  if(check_n_events("fct_b11", 6)) return 1;
  if(check_n_events("fct_c111", 18)) return 1;
  if(check_n_events("fct_d2", 2)) return 1;
  if(check_n_events("fct_e22", 6)) return 1;

  std::cout << "-----\n";
  std::cout << "Wait time (ms) f1_1: " << wait_time_f1_1 << '\n';
  std::cout << "Wait time (ms) f2_1: " << wait_time_f2_1 << '\n';
  std::cout << "Wait time (ms) f1_2: " << wait_time_f1_2 << '\n';
  std::cout << "Wait time (ms) f2_2: " << wait_time_f2_2 << '\n';

  std::cout << "-----\n";
  std::cout << "Wait time (ms) total f1: " << wait_time_f1_1 + wait_time_f1_2 << '\n';
  std::cout << "Wait time (ms) total f2: " << wait_time_f2_1 + wait_time_f2_2 << '\n';

  std::cout << "Cumulated time in fct_b11: " << Timer<0>::get_event_cumul_time("fct_b11") << '\n';

  std::vector<std::string> keys = Timer<0>::get_event_keys();

  auto check_key = [&keys](const std::string& str){
    if(std::find(keys.begin(), keys.end(), str) == keys.end()){
      std::cerr << "Key not found: " << str << '\n';
      return true;
    }
    return false;
  };

  if(check_key("fct_a1"  )) return 1;
  if(check_key("fct_b11" )) return 1;
  if(check_key("fct_c111")) return 1;
  if(check_key("fct_d2"  )) return 1;
  if(check_key("fct_e22" )) return 1;

  if(keys.size() != 5){
    std::cerr << "keys.size() != 5\n";
    return 1;
  }

  Timer<0>::stats_results();
  Timer<0>::pretty_results();

  return 0;
}
